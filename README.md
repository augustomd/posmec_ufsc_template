# README #

Este repositório foi criado com o objetivo de publicar o template que criei para seguir o modelo de dissertação/tese do POSMEC/UFSC.

Parti do modelo da BUUFSC (ufscThesis.cls/BUUFSC.sty), que já estava bastante completo (créditos nos próprios arquivos), e modifiquei conforme guidelines que me foram fornecidas pelo POSMEC. O texto que vem no documento foi em partes copiado do modelo .DOC que me foi fornecido como ponto de partida.

Se desejar contribuir para o desenvolvimento, faça um *pull request*! Não esqueça de manter o código neutro (sem textos pessoais, apenas guidelines). Para utilizar para a sua dissertação, você pode *fork* em seu repositório, ou simplesmente baixar o repositório inteiro (Downloads no menu esquerdo <<<<).

Eu pessoalmente utilizo o TeXNIC center em ambiente Windows. Você não deve ter problemas para utilizar com alguma distribuição linux ou em Mac, entretanto.

A tabela de símbolos está totalmente fora do modelo exigido: corrigirei em breve.